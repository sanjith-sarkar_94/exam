<?php 
/**
 * 
 */
class Admin
{
	private $db;
	private $fm;
	
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}

	public function adminlogin($data){
		$username = $this->fm->validation($data['username']);
		$password = md5($this->fm->validation($data['password']));

		$username = mysqli_real_escape_string($this->db->link, $username);
		$password = mysqli_real_escape_string($this->db->link, $password);

		if ($username == "" || $password == "") {
			$msg = "<div class='btn btn-danger'>Field must not empty!</div>";
			return $msg;
		}

		$sql = "SELECT * FROM db_admin WHERE adminUser = '$username' AND adminPass = '$password'";
		$getadmin = $this->db->select($sql);
		if ($getadmin) {
			$value = $getadmin->fetch_assoc();

			Session::set('login', true);
			Session::set('adminId', $value['adminId']);
			Session::set('adminUser', $value['adminUser']);
			Session::set("loginmsg",  "<div style='text-align:center;'><span class='btn bg-success;'><strong>Success!!</strong> Thank You, You are logged in!!.</span></div>");
			header("Location: index.php");
		}else{
			echo "<span>Sorry! username or password does not matched!</span>";
		}
	}
}
 ?>