<?php 
/**
 * 
 */
class Process
{
	
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}

	public function processData($data){
		$selectedAns = $this->fm->validation($data['ans']);
		$quesNo = $this->fm->validation($data['quesNo']);

		$selectedAns = mysqli_real_escape_string($this->db->link, $selectedAns);
		$quesNo = mysqli_real_escape_string($this->db->link, $quesNo);
		$next = (int)$quesNo + 1;

		if (!isset($_SESSION['score'])) {
				$_SESSION['score'] = '0';
		}

		$getLastRow = $this->getLastRow();
		$getRightAns = $this->getRightAns($quesNo);

		if ($getRightAns == $selectedAns) {
			 $_SESSION['score']++;
		}

		if ($quesNo == $getLastRow) {
			header("Location: final.php");
			exit();
		}else{
			header("Location: test.php?quesNo=".$next);
		}
	}

    
    // Get Last Row from Ans

	public function getLastRow(){
		$sql = "SELECT * FROM tbl_ques";
		$getRows = $this->db->select($sql);
		$totalRows = $getRows->num_rows;
		return $totalRows;
	}

	// Get right ans id by quesNo

	public function getRightAns($id){
    	$sql = "SELECT * FROM tbl_ans WHERE quesNo = '$id' AND rightAns = '1'";
		$getRightAns = $this->db->select($sql)->fetch_assoc();
		$getId = $getRightAns['ansId'];
		return $getId;
    }

    public function getTotalRows(){
		$sql = "SELECT * FROM tbl_ques";
		$getRows = $this->db->select($sql);
		$totalRows = $getRows->num_rows;
		return $totalRows;
	}


	// Get Question Id from Question

	public function getQuestion(){
		$sql = "SELECT * FROM tbl_ques";
		$getRows = $this->db->select($sql);
		return $getRows;
	}

}
 ?>