<?php 
/**
 * 
 */
include_once ("lib/Session.php");
include_once ("config/config.php");
include_once ("lib/Database.php");
include_once ("helpers/Format.php");
class User
{
	private $db;
	private $fm;
	
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}

	public function getAllUser(){
		$sql = "SELECT * FROM tbl_user ORDER BY userid DESC";
		$getUser = $this->db->select($sql);
		return $getUser;
	}

	public function userRegistration($data){
		$name = $data['name'];
		$username = $data['username'];
		$email = $data['email'];
		$password = md5($data['password']);

		if ($name == "" || $username == "" || $email == "" || $password == "") {
			echo "<span class='error'>Field Must Not Empty!</span>";
			exit();
		}elseif (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
			echo "<span class='error'> Invalid Email Address!</span>";
			exit();
		}else{
			$chkquery = "SELECT * FROM tbl_user WHERE email = '$email'";
			$chkemail = $this->db->select($chkquery);
			if ($chkemail != false) {
				echo "<span class='error'> Email Address Already Exit!</span>";
				exit();
			}else{
				$query = "INSERT INTO tbl_user(name, username, email, password) VALUES('$name', '$username', '$email', '$password')";
				$insert_row = $this->db->insert($query);
				if ($insert_row) {
					echo "<span class='Success'> Registration Successfully!</span>";
					exit();
				}else{
					echo "<span class='error'> Not Register!</span>";
					exit();
				}
			}
		}
	}


	// User Login 


	public function userLogin($email, $password){

		$email = $this->fm->validation($email);
		$password = $this->fm->validation($password);

		$username = mysqli_real_escape_string($this->db->link, $email);
		$password = mysqli_real_escape_string($this->db->link, $password);

		if ($email == "" || $password == "") {
			echo "empty";
			exit();
		}else{
			$sql = "SELECT * FROM tbl_user WHERE email = '$email' AND password = '$password'";
			$getUser = $this->db->select($sql);
			if ($getUser != false) {
				$value = $getUser->fetch_assoc();
				if ($value['status'] == '1') {
					echo "disable";
					exit();
				}else{
					Session::init();
					Session::set("login", true);
					Session::set("name", $value['name']);
					Session::set("username", $value['username']);
					Session::set("userid", $value['userid']);
				}
			}else{
				echo "error";
				exit();
			}
		}
	}
}