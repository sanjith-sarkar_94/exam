<?php 
/**
 * 
 */
class Exam
{
	private $db;
	private $fm;
	
	function __construct()
	{
		$this->db = new Database();
		$this->fm = new Format();
	}

	public function getAllQues(){
		$sql = "SELECT * FROM tbl_ques ORDER BY quesId DESC";
		$getQues = $this->db->select($sql);
		return $getQues;
	}

	// Get Total Row from Question

	public function getTotalRows(){
		$sql = "SELECT * FROM tbl_ques";
		$getRows = $this->db->select($sql);
		$totalRows = $getRows->num_rows;
		return $totalRows;
	}


	// Get Question Id from Question

	public function getQuestion(){
		$sql = "SELECT * FROM tbl_ques";
		$getRows = $this->db->select($sql);
		$total = $getRows->fetch_assoc();
		return $total;
	}

	// Get Total Row from Answer

	public function getTotalAns(){
		$sql = "SELECT * FROM tbl_ans";
		$getUser = $this->db->select($sql);
		return $getUser;
	}

	// Insert Question 

	public function insertQues($data){
		$quesNo = $this->fm->validation($data['quesNo']);
		$ques = $this->fm->validation($data['ques']);
		$ans1 = $this->fm->validation($data['ans1']);
		$ans2 = $this->fm->validation($data['ans2']);
		$ans3 = $this->fm->validation($data['ans3']);
		$ans4 = $this->fm->validation($data['ans4']);
		$rightans = $this->fm->validation($data['rightans']);

		$quesNo = mysqli_real_escape_string($this->db->link, $quesNo);
		$ques = mysqli_real_escape_string($this->db->link, $ques);
		$ans1 = mysqli_real_escape_string($this->db->link, $ans1);
		$ans2 = mysqli_real_escape_string($this->db->link, $ans2);
		$ans3 = mysqli_real_escape_string($this->db->link, $ans3);
		$ans4 = mysqli_real_escape_string($this->db->link, $ans4);
		$rightans = mysqli_real_escape_string($this->db->link, $rightans);

		$ans = array();
		$ans[1] = $ans1;
		$ans[2] = $ans2;
		$ans[3] = $ans3;
		$ans[4] = $ans4;

		$sql = "INSERT INTO tbl_ques(quesNo, ques) VALUES('$quesNo', '$ques')";
		$insert_ques = $this->db->insert($sql);
		if ($insert_ques) {
			foreach ($ans as $key => $value) {
				if ($ans != "") {
					if ($rightans == $key) {
						$insert_query = "INSERT INTO tbl_ans(quesNo, rightans, ans) VALUES('$quesNo', '1', '$value')";
					}else{
						$insert_query = "INSERT INTO tbl_ans(quesNo, rightans, ans) VALUES('$quesNo', '0', '$value')";
					}
					$insert_ans = $this->db->insert($insert_query);
					if ($insert_ans) {
						continue;
					}else{
						die('Error..');
					}
				}
			}

			$msg = "<div>Question Added Successfully!</div>";
			return $msg;
		}
	}


	// Get Question by question id

	public function getQuesByQuesNo($quesNo){
		$sql = "SELECT * FROM tbl_ques WHERE quesNo = '$quesNo'";
		$getRows = $this->db->select($sql);
		return $getRows;
	}

    public function getAns($quesNo){
    	$sql = "SELECT * FROM tbl_ans WHERE quesNo = '$quesNo'";
		$getRows = $this->db->select($sql);
		return $getRows;
    }
}
?>