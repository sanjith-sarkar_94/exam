<?php include 'inc/header.php'; 

Session::checkSession();

if (!isset($_GET['quesNo']) || $_GET['quesNo'] == NULL) {
	header("Location exam.php");
}else{
	$quesNo = $_GET['quesNo'];
}

$getAllQues = $exam->getTotalRows();

// Get Question by question no

$getQues = $exam->getQuesByQuesNo($quesNo);

// Get All Answer by question Number

$getans = $exam->getAns($quesNo);

// Question & Ans process 

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
		$process = $pro->processData($_POST);
}
?>
<div class="main">
	<h1>Question <?php echo $quesNo ?> of <?php echo $getAllQues ?></h1>
	<div class="test">
		<form method="POST" action="">
			<table> 	
				<tr>
					<?php if ($getQues) { 
						while ($result = $getQues->fetch_assoc()) { ?>
							
							<td colspan="2">
								<h3><?php echo $result['quesNo'] ?> : <?php echo $result['ques']; ?></h3>
							</td>
						<?php } } ?>
					</tr>
					<?php 
					if ($getans) {
						while ($result = $getans->fetch_assoc()) { ?>
							<tr>
								<td>
									<input type="radio" name="ans" value="<?php echo $result['ansId'] ?>" />
									<?php echo $result['ans'] ?>
								</td>
							</tr>
						<?php }
					}
					?>
					<tr>
						<td>
							<input type="submit" name="submit" value="Next Question"/>
							<input type="hidden" name="quesNo" value="<?php echo $quesNo; ?>" />
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php include 'inc/footer.php'; ?>