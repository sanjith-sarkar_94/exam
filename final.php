<?php include 'inc/header.php'; 
Session::checkSession();
?>
<style>
	.content {
	padding: 30px 0;
	text-align: center;
}
	.main h4 {
	text-align: center;
	border: 1px solid #ddd;
	padding: 18px 0;
	width: 620px;
	margin-left: 100px;
	box-shadow: 2px 2px 0 1px #999;
}
.main h4 a {
	text-decoration: none;
}
</style>
<div class="main">
<h1>You are done!</h1>
	<div class="content">
		<p>Congrats! You have just Completed the test.</p>
		<p>Final Score:
			<?php if (isset($_SESSION['score'])) {
					echo $_SESSION['score'];
					unset($_SESSION['score']);
			} ?>
		</p>
	</div>
	<h4><a href="viewans.php">View-Ans</a></h4>
  </div>
<?php include 'inc/footer.php'; ?>