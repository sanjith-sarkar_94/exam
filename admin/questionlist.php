<?php include 'inc/header.php';



// Get user from tbl_user

$getQues = $ques->getAllQues();


// Delete Product

if (isset($_GET['delid'])) {
	$delid = $_GET['delid'];

	$delproductbyid = $product->delproductbyid($delid);
}
?>
<style>
	.box.round.first.grid {	margin-top: 12px; }
    .odd td { padding: 5px 0;}
    .datatable tr th { text-align: center; padding: 4px 0; background: #b2b2b2; }
    .box h2 { padding-bottom: 18px;}
</style>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Admin Panel - Question List</h2>
		<div class="block" style="text-align: center;">  
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th width="20%">Serial</th>
						<th width="60%">Questions</th>
						<th width="20%">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($getQues) {
						while($result = $getQues->fetch_assoc()){ ?>
							<tr class="odd gradeX">
								<td><?php echo $result['quesNo']; ?></td>
								<td><?php echo $result['ques']; ?></td>
								<td><a onclick="return confirm('are you sure to enable!')" href="?delid=<?php echo $result['productId']; ?>">Remove</a></td>
							</tr>
						<?php	}
					} ?>
				</tbody>
			</table>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		setupLeftMenu();
		$('.datatable').dataTable();
		setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>