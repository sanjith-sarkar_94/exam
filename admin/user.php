<?php include 'inc/header.php';



// Get user from tbl_user

$getUser = $user->getAllUser();


// Delete Product

if (isset($_GET['delid'])) {
	$delid = $_GET['delid'];

	$delproductbyid = $product->delproductbyid($delid);
}
?>
<style>
	.box.round.first.grid {	margin-top: 12px; }
    .odd td { padding: 5px 0;}
    .datatable tr th { text-align: center; padding: 4px 0; background: #b2b2b2; }
    .box h2 { padding-bottom: 18px;}
</style>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Admin Panel - Question List</h2>
		<div class="block" style="text-align: center;">  
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th width="5%">Serial</th>
						<th width="15%">Name</th>
						<th width="20%">Username</th>
						<th width="20%">Email</th>
						<th width="10%">Status</th>
						<th width="20%">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($getUser) {
						while($result = $getUser->fetch_assoc()){ ?>
							<tr class="odd gradeX">
								<td><?php echo $result['userid']; ?></td>
								<td><?php echo $result['name']; ?></td>
								<td><?php echo $result['username']; ?></td>
								<td><?php echo $result['email']; ?></td>
								<td>
									<?php if ($result['status'] == "0") {
										echo "Disable";
									}else{
										echo "Enable";
									} ?>
								</td>
								<td>
									<?php 
									if ($result['status'] == "0") { ?>
										<a onclick="return confirm('are you sure to enable!')" href="?delid=<?php echo $result['productId']; ?>">Enable</a> ||
									<?php }elseif ($result['status'] == "1") { ?>
										 <a onclick="return confirm('are you sure to disable!')" href="?delid=<?php echo $result['productId']; ?>">Disable</a> ||
									<?php } ?>
									<a href="editstatus.php?orderId=<?php echo $result['id']; ?>">Edit</a>
								</td>
							</tr>
						<?php	}
					} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		setupLeftMenu();
		$('.datatable').dataTable();
		setSidebarHeight();
	});
</script>
<?php include 'inc/footer.php';?>