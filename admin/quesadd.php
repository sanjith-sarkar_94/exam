<?php 
include 'inc/header.php';

// Get Number of rows

$getTotalRows = $ques->getTotalRows();
$totalRows = $getTotalRows + 1;

// Insert Question & Answer

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {
	
	$insertQues = $ques->insertQues($_POST);
}
?>
<style>
	 .main h2 { text-align: center;padding-top: 15px;}
	.adminpanel { border: 1px solid #ddd;width: 460px;height: 368px;margin: 30px auto; }
	.tbl { margin: 15px 15px; }
	.tbl tr td{ padding: 3px; }
	.submit { margin-top: 8px;}
</style>
<div class="main">
	<h2>Admin Panel - Add Question</h2>
	<?php if (isset($insertQues)) {
		# code...
		echo $insertQues;
	} ?>
	<div class="adminpanel">
		<form action="" method="POST">
			<table class="tbl">
				<tr>
					<td>Question NO</td>
					<td>:</td>
					<td><input type="number" name="quesNo" readonly value="<?php if(isset($totalRows)){echo $totalRows;} ?>"></td>
				</tr>
				<tr>
					<td>Question</td>
					<td>:</td>
					<td><input type="text" name="ques" placeholder="Enter Question..."></td>
				</tr>
				<tr>
					<td>Choice One</td>
					<td>:</td>
					<td><input type="text" name="ans1" placeholder="Enter Ans One.."></td>
				</tr>
				<tr>
					<td>Choice Two</td>
					<td>:</td>
					<td><input type="text" name="ans2" placeholder="Enter Ans Two.."></td>
				</tr>
				<tr>
					<td>Choice Three</td>
					<td>:</td>
					<td><input type="text" name="ans3" placeholder="Enter Ans Three.."></td>
				</tr>
				<tr>
					<td>Choice Four</td>
					<td>:</td>
					<td><input type="text" name="ans4" placeholder="Enter Ans Four.."></td>
				</tr>
				<tr>
					<td>Correct No</td>
					<td>:</td>
					<td><input type="number" name="rightans" placeholder="Enter Correct Ans No..."></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="submit" name="submit" class="submit" value="Submit"></td>
				</tr>
			</table>
		</form>
	</div>
</div>
<?php 
include 'inc/footer.php';
?>