<?php include 'inc/header.php'; 

Session::checkSession();

$totalRows = $exam->getTotalRows();

// Get All Ans

$getans = $exam->getTotalAns();

// Get All Question

$getQues = $pro->getQuestion();
?>
<div class="main">
	<h1>All Question & Answer <?php echo $totalRows ?></h1>
	<div class="test">
		<form method="POST" action="">
			<table> 	
				<tr>
					<?php if ($getQues) { 
						while ($result = $getQues->fetch_assoc()) { ?>
							
							<td colspan="2">
								<h3><?php echo $result['quesNo'] ?> : <?php echo $result['ques']; ?></h3>
							</td>

						</tr>
						<?php 
						$number = $result['quesNo'];
						$getAns = $exam->getAns($number);

						if ($getAns) {
							while ($result = $getAns->fetch_assoc()) { ?>
								<tr>
									<td>
										<input type="radio" name="ans" />
										<?php 
										if ($result['rightAns'] == '1') {
											echo "<span style='color: red;'>".$result['ans']."</span>";
										}else{
											echo $result['ans'];
										}
										 ?>
									</td>
								</tr>
							<?php }
						}
					}
				}
				?>
			</table>
		</div>
	</div>
	<?php include 'inc/footer.php'; ?>