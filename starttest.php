<?php include 'inc/header.php'; 
Session::checkSession();

$getAllQues = $exam->getTotalRows();

$getQues = $exam->getQuestion();
?>
<style>
	.starttest h2 {
	text-align: center;
	border-bottom: 1px solid #ddd;
	padding: 10px 0;
}
.starttest {
	border: 1px solid #ddd;
	margin: 0 100px;
}
.ansQues p {
	padding: 3px 20px;
	margin-top: 7px;
}
.ansQues ul{ list-style: none; }
.ansQues ul li {
	padding: 2px 20px;															
}
.ansQues {
	margin: 14px 0;
}
.starttest h4 {
	text-align: center;
	border: 1px solid #ddd;
	padding: 14px 0;
	background: #fafaf3;
}
.starttest a {
	text-decoration: none;
}

</style>
<div class="main">
<h1>Welcome to Online Exam</h1>
	<div class="starttest">
	<h2>Test Your Knowledge</h2>
	<div class="ansQues">
		<p>This is multiple choice quiz to test your knowledge</p>
	<ul>
			 <li><strong>Number of Question - </strong><?php echo $getAllQues ?></a></li>
			 <li><strong>Question Type: </strong> Multiple Choice </li>
	</ul>
	</div>
	<h4><a href="test.php?quesNo=<?php echo $getQues['quesNo'] ?>">Start Test</a></h4>
	</div>
	
  </div>
<?php include 'inc/footer.php'; ?>